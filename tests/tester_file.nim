import os, osproc, strutils, parsecsv, streams

const CSVReference = """
valid;test_0;test_1;cfg;duration_ms;output_dir
OK;12;85;0-file_conf.cfg;0;run_0
OK;74;96;1-file_conf.cfg;0;run_1
"""

var (output, errC) = execCmdEx("nim r ./src/kombinator.nim -c tests/file_conf.toml -y")
echo output
if errC != QuitSuccess:
    quit(QuitFailure)

for file in walkDirRec("."):
    if file.contains("kombi_result.csv"):
        var
            result: CsvParser
            reference: CsvParser
            refStream = newStringStream(CSVReference)

        # Parse result
        result.open(file, ';')
        result.readHeaderRow()

        # Parse reference
        reference.open(refStream, "refcsv.csv", ';')
        reference.readHeaderRow()

        # Row 1
        assert result.readRow()
        assert reference.readRow()
        assert result.rowEntry("test_0") == reference.rowEntry("test_0")
        assert result.rowEntry("test_1") == reference.rowEntry("test_1")
        assert result.rowEntry("cfg") == reference.rowEntry("cfg")
        assert result.rowEntry("output_dir") == reference.rowEntry("output_dir")

        # Row 2
        assert result.readRow()
        assert reference.readRow()
        assert result.rowEntry("test_0") == reference.rowEntry("test_0")
        assert result.rowEntry("test_1") == reference.rowEntry("test_1")
        assert result.rowEntry("cfg") == reference.rowEntry("cfg")
        assert result.rowEntry("output_dir") == reference.rowEntry("output_dir")
        
        result.close()
        reference.close()

        # Clean
        let (dir, _, _) = splitFile(file)
        removeDir(dir)
        break
